import { ADDRESS_ZERO } from "./utilities/index";
import {
  connectToContract,
  busdAddr,
  tenThousand,
  binanceCexHotWallet6,
  wbnbAddr,
  usdtAddr,
  getTokenBal,
  one,
  halfa,
  two,
  ten,
  title,
} from "./utilities/utils";
import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";
import hre from "hardhat";
import { BigNumber } from "bignumber.js";
import { anyValue } from "@nomicfoundation/hardhat-chai-matchers/withArgs";
import { parseEther } from "ethers/lib/utils";

describe("Escrow", function () {
  // We define a fixture to reuse the same setup in every test.
  // We use loadFixture to run this setup once, snapshot that state,
  // and reset Hardhat Network to that snapshot in every test.
  async function deployEscrow() {
    /* Contracts are deployed using the first signer/account by default */
    const [ownerCW, client1, provider1, client2, provider2] =
      await ethers.getSigners();

    /* Open whale impersonation request */
    await hre.network.provider.request({
      method: "hardhat_impersonateAccount",
      params: [binanceCexHotWallet6],
    });

    /* Impersonate a whale to gain BUSD balances in the test accounts */
    const binanceCexSigner = await ethers.provider.getSigner(
      binanceCexHotWallet6
    );
    const _busdAsBinanceCex = await hre.ethers.getContractAt(
      "iBEP20",
      busdAddr,
      binanceCexSigner
    ); // Get BUSD contract
    const busdAsBinanceCex = _busdAsBinanceCex.connect(binanceCexSigner); // Get BUSD contract with BinanceCex signer
    await busdAsBinanceCex.transfer(client1.address, tenThousand); // Give client1 initial BUSD balance

    /* Impersonate a whale to gain USDT balances in the test accounts */
    const _usdtAsBinanceCex = await hre.ethers.getContractAt(
      "iBEP20",
      usdtAddr,
      binanceCexSigner
    ); // Get USDT contract
    const usdtAsBinanceCex = _usdtAsBinanceCex.connect(binanceCexSigner); // Get USDT contract with BinanceCex signer
    await usdtAsBinanceCex.transfer(client1.address, tenThousand); // Give client1 initial USDT balance

    /* Close the impersonation request */
    await hre.network.provider.request({
      method: "hardhat_stopImpersonatingAccount",
      params: [binanceCexHotWallet6],
    });

    /* Get token contract objects */
    const busdAsClient1 = await connectToContract("iBEP20", busdAddr, client1); // Get BUSD contract object with client1 signer
    const usdtAsClient1 = await connectToContract("iBEP20", usdtAddr, client1); // Get USDT contract object with client1 signer

    /* Get CanWork contract objects */
    const Escrow = await ethers.getContractFactory("CanWorkEscrowV3c");
    const escrow = await Escrow.deploy();

    return {
      busdAsClient1,
      usdtAsClient1,
      escrow,
      ownerCW,
      client1,
      provider1,
      client2,
      provider2,
    };
  }

  describe("Test - deployment", function () {
    it("Default fee should be 1%", async function () {
      const comparison = new BigNumber("10").pow("18"); // Comparison for 'whole' or 100% is 10**18
      const defaultFee = comparison.div("100"); // 1% of which is the value of the intended default fee for the contract
      const { escrow } = await loadFixture(deployEscrow);

      expect(await escrow.viewDefaultFee()).to.equal(defaultFee);
    });

    it("Should set the correct owner", async function () {
      const { escrow, ownerCW } = await loadFixture(deployEscrow);

      expect(await escrow.owner()).to.equal(ownerCW.address);
    });

    it("Should restrict owner functions", async function () {
      const { escrow, client1 } = await loadFixture(deployEscrow);
      const escrowAsClient = escrow.connect(client1);

      expect(escrowAsClient.releaseByAdmin("0", "0", "0")).to.be.revertedWith(
        "!Auth"
      );
      expect(escrowAsClient.withdrawRevenue()).to.be.revertedWith("!Auth");
      expect(
        escrowAsClient.requestNewOwner(client1.address)
      ).to.be.revertedWith("!Auth");
      expect(escrowAsClient.cancelNewOwner()).to.be.revertedWith("!Auth");
      expect(escrowAsClient.changeDefaultFee("1")).to.be.revertedWith("!Auth");
    });
  });

  describe("Test - depositBNB(provider, JOBID, jobTitle)", function () {
    describe("Validations", function () {
      /* Test validation of require(msg.value > 0, "!Val") */
      it("Should revert if no BNB is sent", async function () {
        const { escrow, client1, provider1 } = await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await expect(
          escrowAsClient.depositBNB(provider1.address, "0", title, {
            value: "0",
          })
        ).to.be.revertedWith("!Val");
      });

      /* Test validation of require(!jobExists[JOBID], "!ID") */
      it("Should revert if job exists already", async function () {
        const { escrow, client1, provider1 } = await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await escrowAsClient.depositBNB(provider1.address, "0", title, {
          value: halfa,
        }); // Create first job (id:0)

        await expect(
          escrowAsClient.depositBNB(provider1.address, "0", title, {
            value: halfa,
          })
        ).to.be.revertedWith("!ID"); // Make sure duplicate job ID reverts
      });

      /* Test validation of require(_finalUSD > 1e18, "!>1USDT") */
      it("Should revert if job is < 1 USDT value", async function () {
        const { escrow, client1, provider1 } = await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await expect(
          escrowAsClient.depositBNB(provider1.address, "0", title, {
            value: "1",
          })
        ).to.be.revertedWith("!>1USDT"); // Make sure low value jobs revert
      });
    });

    describe("Events", function () {
      it("Should emit an event on deposit", async function () {
        const { escrow, client1, provider1 } = await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await expect(
          escrowAsClient.depositBNB(provider1.address, "0", title, {
            value: one,
          })
        )
          .to.emit(escrowAsClient, "Deposit")
          .withArgs(client1.address, provider1.address, anyValue, "0");
      });
    });

    describe("Transfers", function () {
      it("Should transfer the funds to the escrow", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        const initialEscrowBal = await getTokenBal(
          usdtAsClient1,
          escrow.address
        );

        await expect(
          escrowAsClient.depositBNB(provider1.address, "0", title, {
            value: parseEther("1"),
          })
        ).to.changeEtherBalance(client1.address, parseEther("-1"));

        const escrowBal = await usdtAsClient1.balanceOf(escrow.address);
        expect(escrowBal).to.be.greaterThan(initialEscrowBal);
      });
    });

    describe("Mappings", function () {
      it("mapJobToAmount should = actual received amount (via BNB)", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);
        const initialUsdtBal = await getTokenBal(usdtAsClient1, escrow.address);

        await escrowAsClient.depositBNB(provider1.address, "0", title, {
          value: one,
        });
        const afterUsdtBal = await getTokenBal(usdtAsClient1, escrow.address);
        const actualReceived = new BigNumber(afterUsdtBal).minus(
          initialUsdtBal
        );
        const mappingVal = await escrowAsClient.mapJobToAmount("0");
        expect(mappingVal).to.equal(actualReceived);
      });

      it("check all other mappings", async function () {
        const { escrow, client1, provider1 } = await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await escrowAsClient.depositBNB(provider1.address, "0", title, {
          value: one,
        });
        const latestBlock = await hre.ethers.provider.getBlock("latest");

        const jobExists = await escrowAsClient.jobExists("0");
        const mapJobToClient = await escrowAsClient.mapJobToClient("0");
        const mapJobToProvider = await escrowAsClient.mapJobToProvider("0");
        const mapJobToAssetIn = await escrowAsClient.mapJobToAssetIn("0");
        const mapJobToTitle = await escrowAsClient.mapJobToTitle("0");
        const lastTxnBlock = await escrowAsClient.lastTxnBlock("0");
        expect(jobExists).to.equal(true);
        expect(mapJobToClient).to.equal(client1.address);
        expect(mapJobToProvider).to.equal(provider1.address);
        expect(mapJobToAssetIn).to.equal(ADDRESS_ZERO);
        expect(mapJobToTitle).to.equal(title);
        expect(lastTxnBlock).to.equal(latestBlock.number);
      });
    });
  });

  describe("Test - depositBEP20(assetAddr, providerAddr, value, JOBID, jobTitle, swapPath)", function () {
    describe("Validations", function () {
      /* Test validation of require(value > 0, "!Val") */
      it("Should revert if no value is sent", async function () {
        const { escrow, client1, provider1 } = await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await expect(
          escrowAsClient.depositBEP20(
            busdAddr,
            provider1.address,
            "0",
            "0",
            title,
            [usdtAddr]
          )
        ).to.be.revertedWith("!Val");
      });

      /* Test validation of require(!jobExists[JOBID], "!ID") */
      it("Should revert if job exists already", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, ten); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          busdAddr,
          provider1.address,
          two,
          "0",
          title,
          [busdAddr, usdtAddr]
        ); // Create first job (id:0)

        await expect(
          escrowAsClient.depositBEP20(
            busdAddr,
            provider1.address,
            two,
            "0",
            title,
            [busdAddr, usdtAddr]
          )
        ).to.be.revertedWith("!ID"); // Make sure duplicate job ID reverts
      });

      /* Test validation of:
      require(
        iBEP20(asset).transferFrom(msg.sender, address(this), value),
        "!Tx"
      ) */

      /* Test validation of require(swapPath.length > 0, "!SwapPath") */
      it("Should revert if swap path is empty", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, two); // approve BUSD tsf: client1 -> escrow
        await expect(
          escrowAsClient.depositBEP20(
            busdAddr,
            provider1.address,
            two,
            "0",
            title,
            []
          )
        ).to.be.revertedWith("!SwapPath");
      });

      /* Test validation of require(swapPath[0] == asset, "!SwapPathStart") */
      it("Should revert if first node in swap path is not matching", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await expect(
          escrowAsClient.depositBEP20(
            usdtAddr,
            provider1.address,
            two,
            "0",
            title,
            [ADDRESS_ZERO, usdtAddr]
          )
        ).to.be.revertedWith("!SwapPathStart");
      });

      /* Test validation of require(_pathEnd == USDT, "Hx") */
      it("Should revert if final node in swap path !== USDT", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, two); // approve BUSD tsf: client1 -> escrow
        await expect(
          escrowAsClient.depositBEP20(
            busdAddr,
            provider1.address,
            two,
            "0",
            title,
            [busdAddr, wbnbAddr] // USDT addr !== final item in array
          )
        ).to.be.revertedWith("!SwapPathEnd");
      });

      /* Test validation of PancakeLibrary: ZERO_ADDRESS */
      it("Should revert if a node in swap path === Address0", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, ten); // approve BUSD tsf: client1 -> escrow
        await expect(
          escrowAsClient.depositBEP20(
            busdAddr,
            provider1.address,
            ten,
            "0",
            title,
            [busdAddr, ADDRESS_ZERO, usdtAddr]
          )
        ).to.be.revertedWith("PancakeLibrary: ZERO_ADDRESS");
      });

      /* Test validation of PancakeLibrary: IDENTICAL_ADDRESSES */
      it("Should revert if duplicate of addresses in swap path", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, ten); // approve BUSD tsf: client1 -> escrow
        await expect(
          escrowAsClient.depositBEP20(
            busdAddr,
            provider1.address,
            ten,
            "0",
            title,
            [busdAddr, wbnbAddr, wbnbAddr, usdtAddr]
          )
        ).to.be.revertedWith("PancakeLibrary: IDENTICAL_ADDRESSES");
      });

      /* Test validation of require(_finalUSD > 1e18, "!>1USDT") */
      it("Should revert if job value < 1 USDT", async function () {
        const { escrow, client1, provider1, busdAsClient1, usdtAsClient1 } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, "100"); // approve BUSD tsf: client1 -> escrow
        await expect(
          escrowAsClient.depositBEP20(
            busdAddr,
            provider1.address,
            "100",
            "0",
            title,
            [busdAddr, usdtAddr]
          )
        ).to.be.revertedWith("!>1USDT"); // BUSD (swap) resulting in < 1 unit USDT

        await usdtAsClient1.approve(escrow.address, "100"); // approve USDT tsf: client1 -> escrow
        await expect(
          escrowAsClient.depositBEP20(
            usdtAddr,
            provider1.address,
            "100",
            "0",
            title,
            [usdtAddr]
          )
        ).to.be.revertedWith("!>1USDT"); // USDT (no swap) < 1 unit USDT
      });
    });

    describe("Events", function () {
      it("Should emit an event on deposit", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow

        await expect(
          escrowAsClient.depositBEP20(
            usdtAddr,
            provider1.address,
            two,
            "0",
            title,
            [usdtAddr]
          )
        )
          .to.emit(escrow, "Deposit")
          .withArgs(client1.address, provider1.address, two, "0");
      });
    });

    describe("Transfers", function () {
      it("Should transfer the funds to the escrow", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await expect(
          escrowAsClient.depositBEP20(
            usdtAddr,
            provider1.address,
            two,
            "0",
            title,
            [usdtAddr]
          )
        ).to.changeTokenBalances(
          usdtAsClient1,
          [client1.address, escrow.address],
          [parseEther("-2"), parseEther("2")]
        );
      });
    });

    describe("Mappings", function () {
      it("mapJobToAmount should = actual received amount (direct USDT)", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);
        const initialBusdBal = await getTokenBal(usdtAsClient1, escrow.address);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const afterBusdBal = await getTokenBal(usdtAsClient1, escrow.address);
        const actualReceived = new BigNumber(afterBusdBal).minus(
          initialBusdBal
        );
        const intendedReceived = two;
        expect(intendedReceived).to.equal(actualReceived);
        const mappingVal = await escrowAsClient.mapJobToAmount("0");
        expect(mappingVal).to.equal(actualReceived);
      });

      it("mapJobToAmount should = actual received amount (via BUSD)", async function () {
        const { escrow, client1, provider1, busdAsClient1, usdtAsClient1 } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);
        const initialBusdBal = await getTokenBal(usdtAsClient1, escrow.address);

        await busdAsClient1.approve(escrow.address, ten); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          busdAddr,
          provider1.address,
          ten,
          "0",
          title,
          [busdAddr, usdtAddr]
        ); // Create first job (id:0)

        const afterBusdBal = await getTokenBal(usdtAsClient1, escrow.address);
        const actualReceived = new BigNumber(afterBusdBal).minus(
          initialBusdBal
        );
        const mappingVal = await escrowAsClient.mapJobToAmount("0");
        expect(mappingVal).to.equal(actualReceived);
      });

      it("check all other mappings", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, ten); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          busdAddr,
          provider1.address,
          ten,
          "0",
          title,
          [busdAddr, usdtAddr]
        ); // Create first job (id:0)
        const latestBlock = await hre.ethers.provider.getBlock("latest");

        const jobExists = await escrowAsClient.jobExists("0");
        const mapJobToClient = await escrowAsClient.mapJobToClient("0");
        const mapJobToProvider = await escrowAsClient.mapJobToProvider("0");
        const mapJobToAssetIn = await escrowAsClient.mapJobToAssetIn("0");
        const mapJobToTitle = await escrowAsClient.mapJobToTitle("0");
        const lastTxnBlock = await escrowAsClient.lastTxnBlock("0");
        expect(jobExists).to.equal(true);
        expect(mapJobToClient).to.equal(client1.address);
        expect(mapJobToProvider).to.equal(provider1.address);
        expect(mapJobToAssetIn).to.equal(busdAddr);
        expect(mapJobToTitle).to.equal(title);
        expect(lastTxnBlock).to.equal(latestBlock.number);
      });
    });
  });

  describe("Test - releaseAsClient(JOBID)", function () {
    describe("Validations", function () {
      /* Test validation of require(jobExists[JOBID], "!ID") */
      it("Should revert if job does not exist", async function () {
        const { escrow, client1 } = await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await expect(escrowAsClient.releaseAsClient("0")).to.be.revertedWith(
          "!ID"
        );
      });

      /* Test validation of require(mapJobToClient[JOBID] == msg.sender, "!Auth") */
      it("Should revert if non-client calls function", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsProvider = escrow.connect(provider1);
        await expect(escrowAsProvider.releaseAsClient("0")).to.be.revertedWith(
          "!Auth"
        );
      });

      /* Test validation of require(!jobReleased[JOBID], "Rel") */
      it("Should revert if trying to release a job multiple times", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, two); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          busdAddr,
          provider1.address,
          two,
          "0",
          title,
          [busdAddr, usdtAddr]
        ); // Create first job (id:0)

        await escrowAsClient.releaseAsClient("0"); // Release the job as client
        await expect(escrowAsClient.releaseAsClient("0")).to.be.revertedWith(
          "Rel"
        ); // Revert if trying to release an already-released job
      });
    });

    describe("Events", function () {
      it("Should emit an event on release", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const expectedReleaseAmount = new BigNumber(two)
          .times("0.99")
          .toFixed(0);

        await expect(escrowAsClient.releaseAsClient("0"))
          .to.emit(escrow, "Release")
          .withArgs(
            client1.address,
            provider1.address,
            provider1.address,
            expectedReleaseAmount,
            "0",
            ethers.BigNumber.from(1)
          );
      });
    });

    describe("Transfers", function () {
      it("Should transfer the funds to the provider", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const expectedReleaseAmount = new BigNumber(two)
          .times("0.99")
          .toFixed(0);

        await expect(
          escrowAsClient.releaseAsClient("0")
        ).to.changeTokenBalances(
          usdtAsClient1,
          [escrow.address, provider1.address],
          ["-" + expectedReleaseAmount, expectedReleaseAmount]
        );
      });
    });

    describe("Mappings", function () {
      it("adminRevenue should increase by expected amount", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const prevRevenue = (await escrowAsClient.adminRevenue()).toString();
        const expectedFeeAmount = new BigNumber(two).times("0.01").toFixed(0);
        await escrowAsClient.releaseAsClient("0");

        const expectedNewRevAmount = new BigNumber(prevRevenue).plus(
          expectedFeeAmount
        );
        expect(await escrowAsClient.adminRevenue()).to.equal(
          expectedNewRevAmount
        );
      });

      it("all other mappings", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, two); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          busdAddr,
          provider1.address,
          two,
          "0",
          title,
          [busdAddr, usdtAddr]
        ); // Create first job (id:0)

        await escrowAsClient.releaseAsClient("0");
        const latestBlock = await hre.ethers.provider.getBlock("latest");

        expect(await escrowAsClient.jobReleased("0")).to.equal(true);
        expect(await escrowAsClient.closedStatus("0")).to.equal(1);
        expect(await escrowAsClient.lastTxnBlock("0")).to.equal(
          latestBlock.number
        );
      });
    });
  });

  describe("Test - releaseByProvider(JOBID)", function () {
    describe("Validations", function () {
      /* Test validation of require(jobExists[JOBID], "!ID") */
      it("Should revert if job does not exist", async function () {
        const { escrow, provider1 } = await loadFixture(deployEscrow);
        const escrowAsProvider = escrow.connect(provider1);

        await expect(
          escrowAsProvider.releaseByProvider("0")
        ).to.be.revertedWith("!ID");
      });

      /* Test validation of require(mapJobToProvider[JOBID] == msg.sender, "!Auth") */
      it("Should revert if non-provider calls function", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, two); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          busdAddr,
          provider1.address,
          two,
          "0",
          title,
          [busdAddr, usdtAddr]
        ); // Create first job (id:0)

        const escrowAsNonProvider = escrowAsClient;
        await expect(
          escrowAsNonProvider.releaseByProvider("0")
        ).to.be.revertedWith("!Auth");
      });

      /* Test validation of require(!jobReleased[JOBID], "Rel") */
      it("Should revert if trying to release a job multiple times", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, two); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          busdAddr,
          provider1.address,
          two,
          "0",
          title,
          [busdAddr, usdtAddr]
        ); // Create first job (id:0)

        const escrowAsProvider = escrow.connect(provider1);
        await escrowAsProvider.releaseByProvider("0"); // Release the job as client
        await expect(
          escrowAsProvider.releaseByProvider("0")
        ).to.be.revertedWith("Rel"); // Revert if trying to release an already-released job
      });
    });

    describe("Events", function () {
      it("Should emit an event on release with correct values", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsProvider = escrow.connect(provider1);
        await expect(escrowAsProvider.releaseByProvider("0"))
          .to.emit(escrow, "Release")
          .withArgs(
            client1.address,
            provider1.address,
            client1.address,
            two,
            "0",
            ethers.BigNumber.from(2)
          ); // Check here to make sure the final release amount === mapped job amount
      });
    });

    describe("Transfers", function () {
      it("Should transfer the funds to the client", async function () {
        const { escrow, client1, provider1, usdtAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsProvider = escrow.connect(provider1);
        await expect(
          escrowAsProvider.releaseByProvider("0")
        ).to.changeTokenBalances(
          usdtAsClient1,
          [escrow.address, client1.address],
          ["-" + two, two]
        );
      });
    });

    describe("Mappings", function () {
      it("all other mappings", async function () {
        const { escrow, client1, provider1, busdAsClient1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await busdAsClient1.approve(escrow.address, two); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          busdAddr,
          provider1.address,
          two,
          "0",
          title,
          [busdAddr, usdtAddr]
        ); // Create first job (id:0)

        const escrowAsProvider = escrow.connect(provider1);
        await escrowAsProvider.releaseByProvider("0");
        const latestBlock = await hre.ethers.provider.getBlock("latest");

        expect(await escrowAsClient.jobReleased("0")).to.equal(true);
        expect(await escrowAsClient.closedStatus("0")).to.equal(2);
        expect(await escrowAsClient.lastTxnBlock("0")).to.equal(
          latestBlock.number
        );
      });
    });
  });

  describe("Test - releaseByAdmin(JOBID, clientSplit, providerSplit)", function () {
    describe("Validations", function () {
      /* Test validation of require(jobExists[JOBID], "!ID") */
      it("Should revert if job does not exist", async function () {
        const { escrow, ownerCW } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);

        await expect(
          escrowAsAdmin.releaseByAdmin("0", "100", "200")
        ).to.be.revertedWith("!ID");
      });

      /* Test validation of require(!jobReleased[JOBID], "Rel") */
      it("Should revert if trying to release a job multiple times", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        await escrowAsAdmin.releaseByAdmin("0", two, "0"); // Release the job
        await expect(
          escrowAsAdmin.releaseByAdmin("0", "10000", "122")
        ).to.be.revertedWith("Rel"); // Revert if trying to release an already-released job
      });

      /* Test validation of require((totalSplit <= _amount), "TooHigh") */
      it("Should revert if admin tries to pay out amount > job's full deposit amount", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        await expect(
          escrowAsAdmin.releaseByAdmin("0", two, one)
        ).to.be.revertedWith("TooHigh");
      });

      /* Test validation of require((totalSplit >= minPayoutSplit), "TooLow") */
      it("Should revert if admin tries to take high fee", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        await expect(
          escrowAsAdmin.releaseByAdmin("0", halfa, halfa)
        ).to.be.revertedWith("TooLow");
      });
    });

    describe("Events", function () {
      it("Should emit two events on release", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        const expectedAmount = BigNumber(one).times("0.99").toFixed(0);
        await expect(escrowAsAdmin.releaseByAdmin("0", one, one))
          .to.emit(escrowAsAdmin, "Release")
          .withArgs(
            client1.address,
            provider1.address,
            client1.address,
            expectedAmount,
            "0",
            ethers.BigNumber.from(3)
          )
          .to.emit(escrowAsAdmin, "Release")
          .withArgs(
            client1.address,
            provider1.address,
            provider1.address,
            expectedAmount,
            "0",
            ethers.BigNumber.from(3)
          );
      });
    });

    describe("Transfers", function () {
      it("Should transfer the funds to the client", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        const expectedAmount = BigNumber(one).times("0.99").toFixed(0);
        const expectedAmountTotal = BigNumber(expectedAmount).times("2");
        await expect(
          escrowAsAdmin.releaseByAdmin("0", one, one)
        ).to.changeTokenBalances(
          usdtAsClient1,
          [escrow.address, client1.address, provider1.address],
          ["-" + expectedAmountTotal, expectedAmount, expectedAmount]
        );
      });

      it("Should be able to give 0-split to provider", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        await escrowAsAdmin.releaseByAdmin("0", two, "0");
      });

      it("Should be able to give 0-split to client", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        await escrowAsAdmin.releaseByAdmin("0", "0", two);
      });
    });

    describe("Mappings", function () {
      it("all other mappings", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        await escrowAsAdmin.releaseByAdmin("0", "0", two);
        const latestBlock = await hre.ethers.provider.getBlock("latest");

        expect(await escrowAsClient.jobReleased("0")).to.equal(true);
        expect(await escrowAsClient.closedStatus("0")).to.equal(3);
        expect(await escrowAsClient.lastTxnBlock("0")).to.equal(
          latestBlock.number
        );
      });
    });
  });

  describe("Test - withdrawRevenue()", function () {
    describe("Validations", function () {
      /* Test validation of require(_adminRevenue > 0, "!Revenue") */
      it("Should revert if no revenue available", async function () {
        const { escrow, ownerCW } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);

        await expect(escrowAsAdmin.withdrawRevenue()).to.be.revertedWith(
          "!Revenue"
        );
      });
    });

    describe("Events", function () {
      it("Should emit event", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve USDT tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        await escrowAsAdmin.releaseByAdmin("0", one, one);

        const expectedRevenue = await escrow.adminRevenue();

        await expect(escrowAsAdmin.withdrawRevenue())
          .to.emit(escrow, "WithdrawRevenue")
          .withArgs(expectedRevenue, ownerCW.address);
      });
    });

    describe("Transfers", function () {
      it("Should transfer the BUSD to the admin", async function () {
        const { escrow, client1, provider1, usdtAsClient1, ownerCW } =
          await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)

        const escrowAsAdmin = escrow.connect(ownerCW);
        await escrowAsAdmin.releaseByAdmin("0", one, one);

        const expectedRevenue = await escrow.adminRevenue();

        await expect(escrowAsAdmin.withdrawRevenue()).to.changeTokenBalances(
          usdtAsClient1,
          [escrow.address, ownerCW.address],
          ["-" + expectedRevenue, expectedRevenue]
        );
      });
    });

    describe("Mappings", function () {
      // TODO: make sure adminRevenue resets to 0 afterwards
    });
  });

  describe("Test - requestNewOwner(newOwner)", function () {
    describe("Validations", function () {
      /* Test validation of require(newOwner != address(0)) */
      it("Should revert if input is address(0)", async function () {
        const { escrow, ownerCW } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);

        await expect(
          escrowAsAdmin.requestNewOwner(ADDRESS_ZERO)
        ).to.be.revertedWith("!addr0");
      });
    });

    describe("Events", function () {
      it("Should emit PendingOwner event", async function () {
        const { escrow, ownerCW, client1 } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);

        await expect(escrowAsAdmin.requestNewOwner(client1.address))
          .to.emit(escrowAsAdmin, "PendingOwner")
          .withArgs(ownerCW.address, client1.address);
      });
    });
  });

  describe("Test - cancelNewOwner()", function () {
    // describe("Validations", function () {
    // });

    describe("Events", function () {
      it("Should emit PendingOwner event", async function () {
        const { escrow, ownerCW } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);

        await expect(escrowAsAdmin.cancelNewOwner())
          .to.emit(escrowAsAdmin, "PendingOwner")
          .withArgs(ownerCW.address, ADDRESS_ZERO);
      });
    });
  });

  describe("Test - changeOwner()", function () {
    describe("Validations", function () {
      /* Test validation of require(pendingNewOwner != address(0)) */
      it("Should revert if newOwner is address(0)", async function () {
        const { escrow, ownerCW } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);

        await expect(escrowAsAdmin.changeOwner()).to.be.revertedWith("!addr0");
      });

      /* Test validation of require(pendingNewOwner == msg.sender) */
      it("Should revert if newOwner is not the caller", async function () {
        const { escrow, ownerCW, client1 } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);

        await escrowAsAdmin.requestNewOwner(client1.address);
        await expect(escrowAsAdmin.changeOwner()).to.be.revertedWith("!caller");
      });
    });

    describe("Events", function () {
      it("Should emit ChangeOwner event & reset pending owner to address0", async function () {
        const { escrow, ownerCW, client1 } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);
        await escrowAsAdmin.requestNewOwner(client1.address);
        const escrowAsNewOwner = escrow.connect(client1);
        await expect(escrowAsNewOwner.changeOwner())
          .to.emit(escrowAsAdmin, "ChangeOwner")
          .withArgs(ownerCW.address, client1.address);

        expect(await escrowAsNewOwner.pendingNewOwner()).to.equal(ADDRESS_ZERO);
      });
    });
  });

  describe("Test - changeDefaultFee(newFee)", function () {
    describe("Validations", function () {
      /* Test validation of require(newFee != DEFAULT_FEE_PERCENT) */
      it("Should revert if input is same as existing value", async function () {
        const { escrow, ownerCW } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);
        const defaultFee = BigNumber(10).pow(16).toString();

        await expect(
          escrowAsAdmin.changeDefaultFee(defaultFee)
        ).to.be.revertedWith("NoChange");
      });

      /* Test validation of require(newFee > uint(10 ** 16) && newFee < uint(10 ** 18)) */
      it("Should revert if input is not between 1-10 %", async function () {
        const { escrow, ownerCW } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);
        const invalidNewFee1 = BigNumber(10).pow(16).minus("1").toString();
        const invalidNewFee2 = BigNumber(10).pow(17).plus("1").toString();

        await expect(
          escrowAsAdmin.changeDefaultFee(invalidNewFee1)
        ).to.be.revertedWith("!ValidFee");
        await expect(
          escrowAsAdmin.changeDefaultFee(invalidNewFee2)
        ).to.be.revertedWith("!ValidFee");
      });
    });

    describe("Events", function () {
      it("Should emit event", async function () {
        const { escrow, ownerCW } = await loadFixture(deployEscrow);
        const escrowAsAdmin = escrow.connect(ownerCW);
        const defaultFee = BigNumber(10).pow(16).toString();
        const newFee = BigNumber(10).pow(17).toString();

        await expect(escrowAsAdmin.changeDefaultFee(newFee))
          .to.emit(escrowAsAdmin, "ChangeFee")
          .withArgs(defaultFee, newFee);
      });
    });
  });

  describe("Test - getJobs(startId, endId)", function () {
    describe("Validations", function () {
      /* Test validation of require(endId >= startId, "!ascID") */
      it("Should revert if IDs are not in asc-order", async function () {
        const { escrow, client1 } = await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await expect(escrowAsClient.getJobs("9", "0")).to.revertedWith(
          "!ascID"
        );
      });

      /* Test validation of require(endId < jobCounter, "!validID") */
      it("Should revert if endID is too high", async function () {
        const { escrow, client1 } = await loadFixture(deployEscrow);
        const escrowAsClient = escrow.connect(client1);

        await expect(escrowAsClient.getJobs("0", "5")).to.revertedWith(
          "!validID"
        );
      });
    });

    describe("Returns", function () {
      it("Should return correct values", async function () {
        const { escrow, usdtAsClient1, client1, provider1 } = await loadFixture(
          deployEscrow
        );
        const escrowAsClient = escrow.connect(client1);

        await usdtAsClient1.approve(escrow.address, two); // approve BUSD tsf: client1 -> escrow
        await escrowAsClient.depositBEP20(
          usdtAddr,
          provider1.address,
          two,
          "0",
          title,
          [usdtAddr]
        ); // Create first job (id:0)
        const latestBlock = await hre.ethers.provider.getBlock("latest");

        const jobArray = await escrowAsClient.getJobs("0", "0");
        expect(jobArray.jobCount).to.equal("1");
        expect(jobArray.allJobs[0].JOBID).to.equal("0");
        expect(jobArray.allJobs[0].amount).to.equal(two);
        expect(jobArray.allJobs[0].client).to.equal(client1.address);
        expect(jobArray.allJobs[0].provider).to.equal(provider1.address);
        expect(jobArray.allJobs[0].assetIn).to.equal(usdtAsClient1.address);
        expect(jobArray.allJobs[0].released).to.equal(false);
        expect(jobArray.allJobs[0].jobTitle).to.equal(title);
        expect(jobArray.allJobs[0].closedStatus).to.equal(0);
        expect(jobArray.allJobs[0].lastTxnBlock).to.equal(latestBlock.number);
      });
    });

    // describe("Events", function () {
    // });
  });
});

// TODO: Create tests with may job->releases in varying sizes very big and ...
// ... very small to see if we can break the adminRevenue var from parity/truth ...
// ... and/or brick the withdrawRevenue() function. This shouldnt be possible, but ...
// ... proactively if worried we can add a resetRevenue() style function that allows ...
// ... the admin to reset the var to 0
