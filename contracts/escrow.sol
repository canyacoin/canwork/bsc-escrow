// SPDX-License-Identifier: MIT

/*
MIT License

Copyright (c) 2023 CanWork

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

pragma solidity 0.8.16;

// IMPORTS
import "./iBEP20.sol"; // BEP20 Interface
import "./iPancake.sol"; // Pancakeswap Router Interfaces

contract CanWorkEscrowV3c {
    struct Job {
        uint JOBID;
        address client;
        address provider;
        uint amount;
        bytes32 jobTitle;
        bool released;
        uint8 closedStatus;
        uint lastTxnBlock;
        address assetIn;
    }

    address public constant WBNB = 0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c; // Canonical WBNB address used by Pancake
    address public constant USDT = 0x55d398326f99059fF775485246999027B3197955; // Settlement USDT (BSC-USD) contract address
    address public constant ROUTER = 0x10ED43C718714eb63d5aA57B78B54704E256024E; // Pancake V2 ROUTER
    IPancakeRouter01 constant PANCAKESWAP = IPancakeRouter01(ROUTER); // Define PancakeSwap Router

    uint public adminRevenue;
    uint public jobCounter;
    uint[] public jobList;
    address public owner;
    address public pendingNewOwner;

    uint internal DEFAULT_FEE_PERCENT; 
    
    mapping(uint => bool) public jobExists;
    mapping(uint => bool) public jobReleased;
    mapping(uint => uint) public mapJobToAmount;
    mapping(uint => address) public mapJobToClient;
    mapping(uint => address) public mapJobToProvider;
    mapping(uint => address) public mapJobToAssetIn;
    mapping(uint => bytes32) public mapJobToTitle;
    mapping(uint => uint8) public closedStatus; // 0 = Not Released | 1 = Completed | 2 = Cancelled | 3 = Disputed
    mapping(uint => uint) public lastTxnBlock;

    event Deposit(
        address indexed client,
        address indexed provider,
        uint value,
        uint indexed JOBID
    );
    event Release(
        address indexed client,
        address indexed provider,
        address recipient,
        uint value,
        uint indexed JOBID,
        uint8 closedStatus
    );
    event PendingOwner(address oldOwner, address newOwner);
    event ChangeOwner(address indexed oldOwner, address indexed newOwner);
    event ChangeFee(uint oldFee, uint newFee);
    event WithdrawRevenue(uint amount, address recipient);

    constructor() {
        owner = msg.sender;
        DEFAULT_FEE_PERCENT = uint(10 ** 18 / 100); // 1%
    }

    // Only Owner can execute
    modifier onlyAdmin() {
        require(msg.sender == owner, "!Auth");
        _;
    }

    //==================================== DEPOSIT FUNCTIONS =================================//

    // DEPOSIT BNB
    function depositBNB(
        address provider,
        uint JOBID,
        bytes32 jobTitle
    ) external payable {
        require(msg.value > 0, "!Val");

        require(!jobExists[JOBID], "!ID"); // Common 'deposit' re-ent lock
        jobExists[JOBID] = true; // Common 'deposit' re-ent lock

        mapJobToClient[JOBID] = msg.sender;
        mapJobToProvider[JOBID] = provider;
        mapJobToAssetIn[JOBID] = address(0);
        mapJobToTitle[JOBID] = jobTitle;
        lastTxnBlock[JOBID] = block.number;

        // Call internal swap function
        uint _finalUSD = _pancakeSwapBNB(msg.value);
        require(_finalUSD > 1e18, "!>1USDT");
        mapJobToAmount[JOBID] = _finalUSD;
        jobList.push(JOBID);
        jobCounter++;

        emit Deposit(msg.sender, provider, _finalUSD, JOBID);
    }

    // DEPOSIT BEP20
    // Client to Deposit BEP20 asset and PancakeSwap path
    function depositBEP20(
        address asset,
        address provider,
        uint value,
        uint JOBID,
        bytes32 jobTitle,
        address[] calldata swapPath
    ) external {
        require(value > 0, "!Val");

        require(!jobExists[JOBID], "!ID"); // Common 'deposit' re-ent lock
        jobExists[JOBID] = true; // Common 'deposit' re-ent lock

        require(
            iBEP20(asset).transferFrom(msg.sender, address(this), value),
            "!Tx"
        );
        require(swapPath.length > 0, "!SwapPath");
        require(swapPath[0] == asset, "!SwapPathStart"); // Intended asset should be the first in the path
        require(swapPath[swapPath.length - 1] == USDT, "!SwapPathEnd"); // Mandates that the Pancake swap path outputs USDT only!

        mapJobToClient[JOBID] = msg.sender;
        mapJobToProvider[JOBID] = provider;
        mapJobToAssetIn[JOBID] = asset;
        mapJobToTitle[JOBID] = jobTitle;
        lastTxnBlock[JOBID] = block.number;

        uint _finalUSD;
        if (asset == USDT) {
            _finalUSD = value; // Skips the swap if BEP20 token is already === USDT
        } else {
            require(iBEP20(asset).approve(ROUTER, value), "!Aprv"); // Approve Pancake Router to spend the deposited token
            _finalUSD = _pancakeSwapTokens(value, swapPath); // Call internal swap function
        }
        require(_finalUSD > 1e18, "!>1USDT");
        mapJobToAmount[JOBID] = _finalUSD;

        jobList.push(JOBID);
        jobCounter++;

        emit Deposit(msg.sender, provider, _finalUSD, JOBID);
    }

    //==================================== RELEASE FUNCTIONS =================================//

    // RELEASE BY CLIENT
    // Client Releases to transfer to Provider
    function releaseAsClient(uint JOBID) external {
        require(jobExists[JOBID], "!ID");
        require(mapJobToClient[JOBID] == msg.sender, "!Auth");

        require(!jobReleased[JOBID], "Rel"); // Common 'release' re-ent lock
        jobReleased[JOBID] = true; // Common 'release' re-ent lock
        closedStatus[JOBID] = 1;
        lastTxnBlock[JOBID] = block.number;

        // Release Recipient = Provider
        address _recipient = mapJobToProvider[JOBID];
        uint _amount = mapJobToAmount[JOBID];
        uint _finalRelease = _release(_recipient, _amount, true);
        adminRevenue += (_amount - _finalRelease); // Map remaining funds as revenue

        emit Release(
            msg.sender,
            _recipient,
            _recipient,
            _finalRelease,
            JOBID,
            1
        );
    }

    // RELEASE BY PROVIDER
    // Provider Releases to transfer to Client (no fee charged)
    function releaseByProvider(uint JOBID) external {
        require(jobExists[JOBID], "!ID");
        require(mapJobToProvider[JOBID] == msg.sender, "!Auth");

        require(!jobReleased[JOBID], "Rel"); // Common 'release' re-ent lock
        jobReleased[JOBID] = true; // Common 'release' re-ent lock
        closedStatus[JOBID] = 2;
        lastTxnBlock[JOBID] = block.number;

        // Release Recipient = Client
        address _recipient = mapJobToClient[JOBID];
        uint _amount = mapJobToAmount[JOBID];
        uint _finalRelease = _release(_recipient, _amount, false);

        emit Release(
            _recipient,
            msg.sender,
            _recipient,
            _finalRelease,
            JOBID,
            2
        );
    }

    // RELEASE BY ADMIN
    // Admin to call function specifying *payout amount* to Client and Provider
    // This function can result in more than the DEFAULT_FEE_PERCENT being applied
    // however it is limited to ~2x the set fee amount
    // So if 1% would normally be charged, this function could result in a max of ~2%
    function releaseByAdmin(
        uint JOBID,
        uint clientSplit,
        uint providerSplit
    ) external onlyAdmin {
        require(jobExists[JOBID], "!ID");
        require(!jobReleased[JOBID], "Rel"); // Common 'release' re-ent lock
        jobReleased[JOBID] = true; // Common 'release' re-ent lock
        closedStatus[JOBID] = 3;
        lastTxnBlock[JOBID] = block.number;

        uint _amount = mapJobToAmount[JOBID];
        uint minPayoutSplit = _amount -
            ((DEFAULT_FEE_PERCENT * _amount) / (10 ** 18));
        uint totalSplit = clientSplit + providerSplit;
        require((totalSplit <= _amount), "TooHigh");
        require((totalSplit >= minPayoutSplit), "TooLow"); // Extra fee can be charged limited to 2x

        address _client = mapJobToClient[JOBID];
        address _provider = mapJobToProvider[JOBID];

        uint _clientRelease;
        if (clientSplit > 0) {
            _clientRelease = _release(_client, clientSplit, true);
        }
        uint _provRelease;
        if (providerSplit > 0) {
            _provRelease = _release(_provider, providerSplit, true);
        }
        adminRevenue += (_amount - _clientRelease - _provRelease); // Map remaining funds as revenue

        emit Release(_client, _provider, _client, _clientRelease, JOBID, 3);
        emit Release(_client, _provider, _provider, _provRelease, JOBID, 3);
    }

    // Internal function to handle Release
    // Calculates recipient & fee amounts and Transfers funds
    function _release(
        address _recipient,
        uint _amount,
        bool takeFee
    ) internal returns (uint) {
        uint _finalRelease;
        if (_amount > 0) {
            if (takeFee) {
                // _feeAmount can result in 0 if _amount is small enough to cause it to be less than ...
                // ... 1 wei due to rounding down. In these instances the txn will result in no fee
                uint _feeAmount = (DEFAULT_FEE_PERCENT * _amount) / (10 ** 18);
                uint _amountMinusFee = _amount - _feeAmount;
                // It is possible for _amount > (_feeAmount + _amountMinusFee) due ...
                // to wei rounding hence we update the revenue mapping outside this ...
                // ... function based on the (original mapped amount - _finalRelease)
                require(_amount >= _amountMinusFee, "E3");
                _finalRelease = _amountMinusFee;
            } else {
                _finalRelease = _amount;
            }
            require(iBEP20(USDT).transfer(_recipient, _finalRelease));
        }
        return _finalRelease;
    }

    //==================================== PANCAKESWAP =================================//

    // BEP20 liquidated to USDT via PancakeSwap function call `swapExactTokensForTokens`
    function _pancakeSwapTokens(
        uint amountIn,
        address[] memory path
    ) internal returns (uint _finalUSD) {
        uint amountOutMin = 1;
        uint deadline = block.timestamp + 900; // 15 mins

        uint[] memory _amounts = PANCAKESWAP.swapExactTokensForTokens(
            amountIn,
            amountOutMin,
            path,
            address(this),
            deadline
        );
        uint x = _amounts.length - 1; //gets final position of the returned _amounts[] data
        _finalUSD = _amounts[x];
    }

    // BNB liquidation to USDT via PancakeSwap function call `swapExactETHForTokens`
    function _pancakeSwapBNB(uint amountIn) internal returns (uint _finalUSD) {
        uint amountOutMin = 1;
        uint deadline = block.timestamp + 900; // 15 mins
        address[] memory path = new address[](2);
        path[0] = WBNB;
        path[1] = USDT;

        uint[] memory _amount = PANCAKESWAP.swapExactETHForTokens{
            value: amountIn
        }(amountOutMin, path, address(this), deadline);
        _finalUSD = _amount[1];
    }

    //==================================== ADMIN =================================//


    // Admin withdraws their USDT revenue balance
    function withdrawRevenue() external onlyAdmin {
        address _admin = owner; // Cache the admin address
        uint _adminRevenue = adminRevenue; // Cache remaining revenue balance
        require(_adminRevenue > 0, "!Revenue");
        adminRevenue = 0; // Reset revenue to 0 before tsf
        require(iBEP20(USDT).transfer(_admin, _adminRevenue)); // Tsf revenue to admin
        emit WithdrawRevenue(_adminRevenue, _admin); // Emit event
    }

    // Admin makes a pending request to change the contract owner
    function requestNewOwner(address newOwner) external onlyAdmin {
        require(newOwner != address(0), "!addr0");
        pendingNewOwner = newOwner;
        emit PendingOwner(owner, newOwner);
    }

    // Admin clears pending request for a new owner
    function cancelNewOwner() external onlyAdmin {
        pendingNewOwner = address(0);
        emit PendingOwner(owner, address(0));
    }

    // Pending new owner finalizes the change-over
    function changeOwner() external {
        address _pendingNewOwner = pendingNewOwner; // Cache var locally
        require(_pendingNewOwner != address(0), "!addr0");
        require(_pendingNewOwner == msg.sender, "!caller"); // The new owner must be the function caller
        address _oldOwner = owner; // Cache old owner for event
        owner = _pendingNewOwner;
        pendingNewOwner = address(0);
        emit ChangeOwner(_oldOwner, _pendingNewOwner);
    }

    // Change Default Fee
    function changeDefaultFee(uint newFee) external onlyAdmin {
        uint _oldFee = DEFAULT_FEE_PERCENT; // Cache old fee
        require(newFee != _oldFee, "NoChange");
        require(
            newFee >= uint(10 ** 16) && newFee <= uint(10 ** 17),
            "!ValidFee"
        );
        DEFAULT_FEE_PERCENT = newFee;
        emit ChangeFee(_oldFee, newFee);
    }

    //==================================== UTILS =================================//

    // Returns Job Details
    function getJobs(
        uint startId,
        uint endId
    ) external view returns (uint jobCount, Job[] memory allJobs) {
        require(endId >= startId, "!ascID");
        require(endId < jobCounter, "!validID");
        uint _jobCount = endId - startId + 1;
        Job[] memory jobArray = new Job[](_jobCount);

        for (uint i = startId; i < endId + 1; i++) {
            uint _ID = jobList[i];
            Job memory j;

            j.JOBID = _ID;
            j.amount = mapJobToAmount[_ID];
            j.client = mapJobToClient[_ID];
            j.provider = mapJobToProvider[_ID];
            j.assetIn = mapJobToAssetIn[_ID];
            j.released = jobReleased[_ID];
            j.jobTitle = mapJobToTitle[_ID];
            j.closedStatus = closedStatus[_ID];
            j.lastTxnBlock = lastTxnBlock[_ID];
            jobArray[i] = j;
        }
        jobCount = _jobCount;
        allJobs = jobArray;
    }

    // View Default Fee
    function viewDefaultFee() external view returns (uint defaultFee) {
        defaultFee = DEFAULT_FEE_PERCENT;
    }
}