# BSC Escrow for CanWork (v3)

![Colourful Digital Illustration. Freelancers, Laptops, Lock & Key.  CanWork Escrow V3.  Extra Security](https://firebasestorage.googleapis.com/v0/b/can-work-io.appspot.com/o/uploads%2Farticles%2F57439e8ad628411b91ee6e56f9e7809b.jpg?alt=media)

### USDT-Hedged Escrow on BNB SmartChain (BSC) for CanWork

This is a Solidity smart contract for the escrow service "CanWorkEscrowV3". It utilizes the BEP20 token standard and PancakeSwap for token swapping. 

_The contract allows users to deposit funds (BNB or BEP20 tokens), which will then be converted to USDT, and held in escrow until the client or provider releases the funds._

#### The contract defines several key functionalities:

`Deposit BNB or BEP20 tokens` 
Clients can deposit BNB or BEP20 tokens into the escrow service with a specified job ID. The deposited funds will be converted to USDT using PancakeSwap.

`PancakeSwap calls`
The contract includes internal functions to perform token swaps using PancakeSwap.

All tokens are liquidated to USDT (BEP20) -- hedged as stablecoin for the duration of the job period at the agreed price.

`Release funds by client or provider`
Clients or providers can release the funds held in escrow. The provider (freelancer) will pay a fee (default 1% of the escrow amount) when funds are released, whereas the client will not be charged a fee.

Providers are paid in USDT (BEP20).

`Release funds by admin`
In event of dispute or arbitration, the contract owner (admin) can release the funds in escrow by specifying the payout amount for the client and provider.

Default fee is paid by both client & provider in this instance.

Crucially, admin privilege is limited insofar as bad admin or compromised admin can at most:
- not release funds
- release funds unfairly or arbitrarily.

Admin cannot drain funds or gain extra revenue when arbitrating on behalf of client & freelancer.


###`Admin functions`###
The contract owner has the ability to:
- withdraw their revenue (meaning fees taken)
- request a new owner
- change the default fee (to a maximum of 10%)
- view job details

https://firebasestorage.googleapis.com/v0/b/can-work-io.appspot.com/o/uploads%2Farticles%2Frug.jpg?alt=media


```
// Admin withdraws their USDT revenue balance
    function withdrawRevenue() external onlyAdmin {
        address _admin = owner; // Cache the admin address
        uint _adminRevenue = adminRevenue; // Cache remaining revenue balance
        require(_adminRevenue > 0, "!Revenue");
        adminRevenue = 0; // Reset revenue to 0 before tsf
        require(iBEP20(USDT).transfer(_admin, _adminRevenue)); // Tsf revenue to admin
        emit WithdrawRevenue(_adminRevenue, _admin); // Emit event
    }
```

`adminRevenue` is stored on-chain and updated upon every release
It is defined as the difference of the job amount minus what is paid out to freelancer. That is, the fee amount which is 1%.
```adminRevenue += (_amount - _clientRelease - _provRelease); // Map remaining funds as revenue
```

Crucially, admin cannot remove more than 2 x fee taken, thereby protecting the contract from bad actor or admin compromise.

#### Mainnet Deployment
Curently, deployed on BNB Chain mainnet.
Contract: [https://bscscan.com/address/0x74a012e9eaf79e886f36db8a617a1351576b3b78#code]( https://bscscan.com/address/0x74a012e9eaf79e886f36db8a617a1351576b3b78#code)

### How to use
Try running some of the following tasks:

```shell
npx hardhat help
yarn compile (npx hardhat compile)
yarn test (npx hardhat test)
yarn node (npx hardhat node)
yarn coverage (npx hardhat coverage)
```
